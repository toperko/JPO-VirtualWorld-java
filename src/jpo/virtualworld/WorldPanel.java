package jpo.virtualworld;

import jpo.virtualworld.world.World;

import javax.swing.*;
import java.awt.*;

public class WorldPanel extends JPanel {
    /**
     * The width of the panel.
     */
    private static final int WIDTH = 400;

    /**
     * The height of the panel.
     */
    private static final int HEIGHT = 400;

    private World world;

    public void setWorld(World world) {
        this.world = world;
    }

    public WorldPanel() {
        setBackground(Color.BLACK);
        Dimension size = new Dimension(WIDTH, HEIGHT);
        setMaximumSize(size);
        setMinimumSize(size);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int size = WIDTH / world.getMap().length;
        for (int y = 0; y < world.getMap().length; y++) {
            for (int x = 0; x < world.getMap()[y].length; x++) {
                if(world.getMap()[x][y] == null){
                    g.setColor(Color.black);
                }else{
                    g.setColor(world.getMap()[x][y].getColor());
                }
                g.fillRect(x*size, y*size, size, size);
            }
        }
    }
}
