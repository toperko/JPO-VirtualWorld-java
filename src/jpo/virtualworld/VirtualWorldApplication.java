package jpo.virtualworld;

import jpo.virtualworld.world.World;
import jpo.virtualworld.world.WorldCreator;

import javax.swing.*;
import java.util.Random;

public class VirtualWorldApplication {

    public static void main(String[] args) {
        Random randomGenerator = new Random();

        World world = new World(randomGenerator);
        WorldCreator worldCreator = new WorldCreator(world, randomGenerator);
        worldCreator.run();

        JFrame frame = new JFrame("App");
        frame.setContentPane(new App(world).mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
