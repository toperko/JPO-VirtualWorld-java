package jpo.virtualworld.world;

import static jpo.virtualworld.Parameter.WORLD_HEIGHT;
import static jpo.virtualworld.Parameter.WORLD_WIDTH;

public class Coordinate {
    private int width;
    private int height;

    public Coordinate() {
        this.width = 0;
        this.height = 0;
    }

    public Coordinate(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void set(Coordinate copy) throws CoordinateException {
        setWidth(copy.getWidth());
        setHeight(copy.getHeight());
    }

    public Coordinate(Coordinate original) {
        this(original.getWidth(), original.getHeight());
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) throws CoordinateException {
        if(width < 0 || width >= WORLD_WIDTH)
            throw new CoordinateException();
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) throws CoordinateException {
        if(height < 0 || height >= WORLD_HEIGHT)
            throw new CoordinateException();
        this.height = height;
    }

    public static boolean checkCoordinate(int width, int height) {
        return (width >= 0) && (height >= 0) && (width < WORLD_WIDTH) && (height < WORLD_HEIGHT);
    }
}
