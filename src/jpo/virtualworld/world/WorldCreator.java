package jpo.virtualworld.world;

import jpo.virtualworld.Parameter;
import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.organism.animal.*;
import jpo.virtualworld.organism.plant.*;

import java.util.Random;

public class WorldCreator {

    private World world;
    private Random randomGenerator;

    public WorldCreator(World world, Random randomGenerator){
        this.world = world;
        this.randomGenerator = randomGenerator;
    }

    public void run(){
        int randomCount = randomGenerator.nextInt(10) + 5;
        for(int i = 0; i < randomCount; i++){
            Organism organism = new Grass(world, getRandomCoordinate());
            world.addOrganism(organism, organism.getCoordinate().getWidth(), organism.getCoordinate().getHeight());
        }

        randomCount = randomGenerator.nextInt(2) + 1;
        for(int i = 0; i < randomCount; i++){
            Organism organism = new Thorn(world, getRandomCoordinate());
            world.addOrganism(organism, organism.getCoordinate().getWidth(), organism.getCoordinate().getHeight());
        }

        randomCount = randomGenerator.nextInt(4) + 1;
        for(int i = 0; i < randomCount; i++){
            Organism organism = new Wolfberry(world, getRandomCoordinate());
            world.addOrganism(organism, organism.getCoordinate().getWidth(), organism.getCoordinate().getHeight());
        }

        randomCount = randomGenerator.nextInt(4) + 1;
        for(int i = 0; i < randomCount; i++){
            Organism organism = new Antelope(world, getRandomCoordinate());
            world.addOrganism(organism, organism.getCoordinate().getWidth(), organism.getCoordinate().getHeight());
        }

        randomCount = randomGenerator.nextInt(2) + 1;
        for(int i = 0; i < randomCount; i++){
            Organism organism = new Lion(world, getRandomCoordinate());
            world.addOrganism(organism, organism.getCoordinate().getWidth(), organism.getCoordinate().getHeight());
        }

        randomCount = randomGenerator.nextInt(3) + 2;
        for(int i = 0; i < randomCount; i++){
            Organism organism = new Mosquito(world, getRandomCoordinate());
            world.addOrganism(organism, organism.getCoordinate().getWidth(), organism.getCoordinate().getHeight());
        }

        randomCount = randomGenerator.nextInt(3) + 2;
        for(int i = 0; i < randomCount; i++){
            Organism organism = new Sheep(world, getRandomCoordinate());
            world.addOrganism(organism, organism.getCoordinate().getWidth(), organism.getCoordinate().getHeight());
        }

        randomCount = randomGenerator.nextInt(3) + 2;
        for(int i = 0; i < randomCount; i++){
            Organism organism = new Wolf(world, getRandomCoordinate());
            world.addOrganism(organism, organism.getCoordinate().getWidth(), organism.getCoordinate().getHeight());
        }
    }

    private Coordinate getRandomCoordinate(){
        int x, y;
        try {
            do {
                x = randomGenerator.nextInt(Parameter.WORLD_WIDTH - 1);
                y = randomGenerator.nextInt(Parameter.WORLD_HEIGHT - 1);
            } while (world.isSet(x, y));

            return new Coordinate(x, y);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return new Coordinate();
    }
}
