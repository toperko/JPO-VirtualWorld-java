package jpo.virtualworld.world;

import com.sun.tools.corba.se.idl.constExpr.Or;
import jpo.virtualworld.Parameter;
import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.organism.OrganismComparator;
import jpo.virtualworld.organism.plant.Grass;


import java.util.*;

public class World {
    private Organism[][] world;
    private int livingOrganism;
    private int round;
    private Random randomGenerator;
    private List<Organism> organismList = new ArrayList<Organism>();
    private List<String> communicates = new ArrayList<String>();


    public World(Random randomGenerator) {
        this.randomGenerator = randomGenerator;
        this.world = new Organism[Parameter.WORLD_WIDTH][Parameter.WORLD_HEIGHT];


    }

    public Organism[][] getMap() {
        return world;
    }

    public boolean isSet(int width, int height){
        return Coordinate.checkCoordinate(width, height) && world[width][height] != null;
    }

    public void setOrganism(Organism organism, int x, int y) {
        world[x][y] = organism;
    }

    public void unsetOrganism(int x, int y) {
        world[x][y] = null;
    }

    public Organism getOrganism(int x, int y){
        return world[x][y];
    }

    public void addOrganism(Organism organism, int width, int height) {
        livingOrganism++;
        world[width][height] = organism;
    }

    public void deleteOrganism(int x, int y){
        livingOrganism--;
        unsetOrganism(x, y);
    }

    public void nextRound() {
        round++;
        for (int y = 0; y < world.length; y++) {
            for (int x = 0; x < world[y].length; x++) {
                if (world[y][x] instanceof Organism)
                    organismList.add(world[y][x]);
            }
        }
        organismList.sort(new OrganismComparator());
        for (Iterator<Organism> iterator = organismList.listIterator(); iterator.hasNext(); ) {
            Organism organism = iterator.next();
            if (organism == world[organism.getCoordinate().getWidth()][organism.getCoordinate().getHeight()]) {
                organism.incAge();
                organism.move(randomGenerator);
            }
            iterator.remove();
        }
        Collections.reverse(communicates);
    }

    public void addToCommunicates(String communicate) {
        this.communicates.add(communicate + "\n");
    }

    public int getLivingOrganism() {
        return livingOrganism;
    }

    public Random getRandomGenerator() {
        return randomGenerator;
    }

    public List<String> getCommunicates() {
        return communicates;
    }

    public int getRound() {
        return round;
    }

    public boolean colisionOn(Organism attacker, int x, int y) {
        return world[x][y].collision(attacker);
    }
}
