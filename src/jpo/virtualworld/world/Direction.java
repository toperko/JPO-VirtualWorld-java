package jpo.virtualworld.world;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum Direction {
    up,
    right,
    down,
    left;

    private static final List<Direction> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();

    public static Direction randomDirection(Random randomGenerator)  {
        return VALUES.get(randomGenerator.nextInt(SIZE));
    }
}
