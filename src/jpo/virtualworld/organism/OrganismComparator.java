package jpo.virtualworld.organism;

import java.util.Comparator;

public class OrganismComparator implements Comparator<Organism>{
    @Override
    public int compare(Organism o1, Organism o2) {
        if(o1.getInitiative() < o2.getInitiative()){
            return 1;
        }else if(o1.getInitiative() > o2.getInitiative()){
            return -1;
        }
        return Integer.compare(o1.getAge(), o2.getAge());
    }
}
