package jpo.virtualworld.organism.animal;

import jpo.virtualworld.Parameter;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.World;

import java.awt.*;

public class Wolf extends Animal {
    public Wolf(World world, Coordinate coordinate) {
        super(world, coordinate, Parameter.WOLF_STRENGTH, Parameter.WOLF_INITIATIVE);
    }

    @Override
    public String getName() {
        return "Wilk";
    }

    @Override
    public Color getColor() {
        return Color.RED;
    }
}
