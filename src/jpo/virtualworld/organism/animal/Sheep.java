package jpo.virtualworld.organism.animal;

import jpo.virtualworld.Parameter;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.World;

import java.awt.*;

public class Sheep extends Animal {
    public Sheep(World world, Coordinate coordinate) {
        super(world, coordinate, Parameter.SHEEP_STRENGTH, Parameter.SHEEP_INITIATIVE);
    }

    @Override
    public String getName() {
        return "Owca";
    }

    @Override
    public Color getColor() {
        return Color.lightGray;
    }
}
