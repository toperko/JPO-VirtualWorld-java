package jpo.virtualworld.organism.animal;

import jpo.virtualworld.Parameter;
import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.World;

import java.awt.*;

public class Lion extends Animal {
    public Lion(World world, Coordinate coordinate) {
        super(world, coordinate, Parameter.LION_STRENGTH, Parameter.LION_INITIATIVE);
    }

    @Override
    public String getName() {
        return "Lew";
    }


    @Override
    public Color getColor() {
        return Color.yellow;
    }

    @Override
    public boolean collision(Organism attacker) {
        if (attacker.getStrength() < 5) {
            world.addToCommunicates(attacker.getName() + " poddaje sie przed lwem ");
            return false;
        }
        return super.collision(attacker);
    }
}
