package jpo.virtualworld.organism.animal;

import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.Direction;
import jpo.virtualworld.world.World;

import java.util.Random;

public abstract class Animal extends Organism {

    public Animal(World world, Coordinate coordinate, int strength, int initiative) {
        super(world, coordinate, strength, initiative);
    }

    @Override
    public void move(Random randomGenerator) {
        int x, y;
        do {
            x = this.coordinate.getWidth();
            y = this.coordinate.getHeight();
            switch (Direction.randomDirection(randomGenerator)) {
                case up:
                    y -= 1;
                    break;
                case right:
                    x += 1;
                    break;
                case down:
                    y += 1;
                    break;
                case left:
                    x -= 1;
                    break;
            }
        } while (!Coordinate.checkCoordinate(x, y));
        try {
            if (world.isSet(x, y)) {
                world.addToCommunicates("Kolizja");
                if (world.colisionOn(this, x, y)) {
                    world.unsetOrganism(getCoordinate().getWidth(), getCoordinate().getHeight());
                    getCoordinate().setWidth(x);
                    getCoordinate().setHeight(y);
                    world.setOrganism(this, x, y);
                } else {
                    world.deleteOrganism(getCoordinate().getWidth(), getCoordinate().getHeight());
                }
                return;
            }
            world.unsetOrganism(getCoordinate().getWidth(), getCoordinate().getHeight());
            getCoordinate().setWidth(x);
            getCoordinate().setHeight(y);
            world.setOrganism(this, x, y);
            world.addToCommunicates(getName() + " przesuwa sie");
        }catch (Exception e){
            world.addToCommunicates(e.getMessage());
        }
    }

    @Override
    public boolean collision(Organism attacker) {
        world.addToCommunicates(attacker.getName() + " atakuje " + getName());
        if (attacker.getStrength() > this.getStrength()) {
            world.addToCommunicates(attacker.getName() + " zwycieza ");
            world.deleteOrganism(getCoordinate().getWidth(), getCoordinate().getHeight());
            return true;
        } else {
            world.addToCommunicates(getName() + " zwycieza ");
            return false;
        }
    }

}
