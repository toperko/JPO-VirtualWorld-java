package jpo.virtualworld.organism.animal;

import jpo.virtualworld.Parameter;
import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.World;

import java.awt.*;
import java.util.Random;

public class Mosquito extends Animal {

    public Mosquito(World world, Coordinate coordinate) {
        super(world, coordinate, Parameter.MOSQUITO_STRENGTH, Parameter.MOSQUITO_INITIATIVE);
        oldCoordinate = new Coordinate(coordinate);
    }

    private Coordinate oldCoordinate;

    @Override
    public String getName() {
        return "Komar";
    }

    @Override
    public Color getColor() {
        return Color.MAGENTA;
    }

    @Override
    public int getInitiative() {
        return super.getInitiative() + getCompanion();
    }

    @Override
    public int getStrength() {
        return super.getStrength() + getCompanion();
    }

    private int getCompanion() {
        int x, y, s = 0;
        x = getCoordinate().getWidth();
        y = getCoordinate().getHeight();
        try {
            if (world.isSet(x + 1, y) && world.getOrganism(x + 1, y) instanceof Mosquito) {
                s++;
            }
            if (world.isSet(x, y + 1) && world.getOrganism(x, y + 1) instanceof Mosquito) {
                s++;
            }
            if (world.isSet(x - 1, y) && world.getOrganism(x - 1, y) instanceof Mosquito) {
                s++;
            }
            if (world.isSet(x, y - 1) && world.getOrganism(x, y - 1) instanceof Mosquito) {
                s++;
            }
        } catch (Exception e) {
            world.addToCommunicates(e.toString());
        }
        return s;
    }

    @Override
    public void move(Random randomGenerator) {
        try {
            this.oldCoordinate.set(this.coordinate);
        } catch (Exception e) {
            world.addToCommunicates(e.toString());
        }
        super.move(randomGenerator);
    }

    @Override
    public boolean collision(Organism attacker) {
        world.addToCommunicates(attacker.getName() + " atakuje " + getName());
        if (attacker.getStrength() > this.getStrength()) {
            world.addToCommunicates(attacker.getName() + " zwycieza ");

            if (world.getRandomGenerator().nextInt(1) == 1) {
                world.addToCommunicates(getName() + " ratuje się");
                try {
                    this.coordinate.set(this.oldCoordinate);
                } catch (Exception e) {
                    world.addToCommunicates(e.toString());
                }
                world.setOrganism(this, getCoordinate().getWidth(), getCoordinate().getHeight());
            } else {
                world.addToCommunicates("Komarowi nie udalo sie uciec");
                world.deleteOrganism(getCoordinate().getWidth(), getCoordinate().getHeight());
            }

            return true;
        } else {
            world.addToCommunicates(getName() + " zwycieza ");
            return false;
        }
    }
}
