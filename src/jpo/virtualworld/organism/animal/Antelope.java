package jpo.virtualworld.organism.animal;

import jpo.virtualworld.Parameter;
import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.Direction;
import jpo.virtualworld.world.World;

import java.awt.*;
import java.util.Random;

public class Antelope extends Animal {

    public Antelope(World world, Coordinate coordinate) {
        super(world, coordinate, Parameter.ANTELOPE_STRENGTH, Parameter.ANTELOPE_INITIATIVE);
    }

    @Override
    public String getName() {
        return "Antylopa";
    }

    @Override
    public void move(Random randomGenerator) {
        int x, y;
        do {
            x = this.coordinate.getWidth();
            y = this.coordinate.getHeight();
            switch (Direction.randomDirection(randomGenerator)) {
                case up:
                    y -= 2;
                    break;
                case right:
                    x += 2;
                    break;
                case down:
                    y += 2;
                    break;
                case left:
                    x -= 2;
                    break;
            }
        } while (!Coordinate.checkCoordinate(x, y));
        try {
            if (world.isSet(x, y)) {
                world.addToCommunicates("Kolizja");
                if (world.colisionOn(this, x, y)) {
                    world.unsetOrganism(getCoordinate().getWidth(), getCoordinate().getHeight());
                    getCoordinate().setWidth(x);
                    getCoordinate().setHeight(y);
                    world.setOrganism(this, x, y);
                } else {
                    world.deleteOrganism(getCoordinate().getWidth(), getCoordinate().getHeight());
                }
                return;
            }
            world.unsetOrganism(getCoordinate().getWidth(), getCoordinate().getHeight());
            getCoordinate().setWidth(x);
            getCoordinate().setHeight(y);
            world.setOrganism(this, x, y);
            world.addToCommunicates(getName() + " przesuwa sie");
        } catch (Exception e) {
            world.addToCommunicates(e.getMessage());
        }
    }

    @Override
    public boolean collision(Organism attacker) {
        if (world.getRandomGenerator().nextInt(1) == 0) {
            world.addToCommunicates(getName() + " nie ucieka przed " + attacker.getName());
            return super.collision(attacker);
        }
        int x, y;
        x = this.coordinate.getWidth();
        y = this.coordinate.getHeight();
        try {
            if (!world.isSet(x + 1, y)) {
                x += 1;
            } else if (!world.isSet(x, y - 1)) {
                y -= 1;
            } else if (!world.isSet(x - 1, y)) {
                x -= 1;
            } else if (!world.isSet(x, y + 1)) {
                y += 1;
            } else {
                world.addToCommunicates(getName() + " nie ma gdzie uciec przed " + attacker.getName());
                return super.collision(attacker);
            }
            world.addToCommunicates(getName() + " ucieka przed " + attacker.getName());

            world.unsetOrganism(this.coordinate.getWidth(), this.coordinate.getHeight());
            this.coordinate.setWidth(x);
            this.coordinate.setHeight(y);
            world.setOrganism(this, x, y);
        }catch (Exception e){
            world.addToCommunicates(e.toString());
        }
        return true;
    }

    @Override
    public Color getColor() {
        return Color.pink;
    }
}
