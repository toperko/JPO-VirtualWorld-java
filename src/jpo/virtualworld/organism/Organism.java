package jpo.virtualworld.organism;

import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.World;

import java.awt.*;
import java.util.Random;

public abstract class Organism {
    protected World world;
    protected Coordinate coordinate;
    private int strength;
    private int initiative;
    private int age;

    public Organism(World world, Coordinate coordinate, int strength, int initiative) {
        this.world = world;
        this.coordinate = coordinate;
        this.strength = strength;
        this.initiative = initiative;
        this.age = 0;
    }

    public Organism(Organism original) {
        this.world = original.world;
        this.coordinate = new Coordinate(original.coordinate);
        this.strength = original.strength;
        this.initiative = original.initiative;
        this.age = 0;
    }


    public abstract void move(Random randomGenerator);

    public abstract boolean collision(Organism attacker);

    public abstract Color getColor();

    public abstract String getName();

    public int getStrength() {
        return this.strength;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public int getInitiative() {
        return initiative;
    }

    public int getAge() {
        return age;
    }

    public void incAge() {
        age++;
    }

}
