package jpo.virtualworld.organism.plant;

import jpo.virtualworld.Parameter;
import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.Direction;
import jpo.virtualworld.world.World;

import java.awt.*;
import java.util.Random;

public class Thorn extends Plant {
    public Thorn(World world, Coordinate coordinate) {
        super(world, coordinate, Parameter.THORN_STRENGTH);
    }

    public Thorn(Thorn thorn) {
        super(thorn);
    }

    @Override
    public String getName() {
        return "Ciern";
    }

    @Override
    protected int getGrowthChance() {
        return Parameter.THORN_GROWTH_CHANCE;
    }

    @Override
    public Color getColor() {
        return Color.darkGray;
    }

    @Override
    public Organism newOrganism() {
        return new Thorn(this);
    }

    @Override
    public void move(Random randomGenerator) {
        if (!isGrowth(randomGenerator)) {
            return;
        }
        int x, y, i = 0;
        try {
            do {
                x = this.coordinate.getWidth();
                y = this.coordinate.getHeight();
                switch (Direction.randomDirection(randomGenerator)) {
                    case up:
                        y -= 1;
                        break;
                    case right:
                        x += 1;
                        break;
                    case down:
                        y += 1;
                        break;
                    case left:
                        x -= 1;
                        break;
                }
            } while (++i < 4 //ograniczenie ilosci prob
                    && !(
                    Coordinate.checkCoordinate(x, y)
                            && (!world.isSet(x, y) || world.getOrganism(x, y) instanceof Thorn) //ograniczenie kanibalizmu
            )
                    );
            if (!Coordinate.checkCoordinate(x, y) || (world.isSet(x, y) && world.getOrganism(x, y) instanceof Thorn)) {
                return;
            }
            if (world.isSet(x, y)) {
                world.addToCommunicates("Ciernie rozrastają sie i zabijaja " + world.getOrganism(x, y).getName());
                world.deleteOrganism(x, y);
            } else {
                world.addToCommunicates("Ciernie rozrastają sie");
            }
            Organism newOrganism = this.newOrganism();
            newOrganism.getCoordinate().setWidth(x);
            newOrganism.getCoordinate().setHeight(y);
            world.addOrganism(newOrganism, x, y);
        } catch (Exception e) {
            world.addToCommunicates(e.toString());
        }
    }
}
