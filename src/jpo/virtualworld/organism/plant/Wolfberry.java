package jpo.virtualworld.organism.plant;

import jpo.virtualworld.Parameter;
import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.World;

import java.awt.*;

public class Wolfberry extends Plant {
    public Wolfberry(World world, Coordinate coordinate) {
        super(world, coordinate, Parameter.WOLFBERRY_STRENGTH);
    }

    public Wolfberry(Wolfberry wolfberry) {
        super(wolfberry);
    }

    @Override
    public String getName() {
        return "Wilcze Jagody";
    }

    @Override
    protected int getGrowthChance() {
        return Parameter.WOLFBERRY_GROWTH_CHANCE;
    }

    @Override
    public Color getColor() {
        return Color.blue;
    }

    @Override
    public Organism newOrganism() {
        return new Wolfberry(this);
    }

    @Override
    public boolean collision(Organism attacker) {
        world.deleteOrganism(this.getCoordinate().getWidth(), this.getCoordinate().getHeight());
        world.addToCommunicates(getName() + " zostala zjedzona przez " + attacker.getName());
        world.addToCommunicates(attacker.getName() + " ginie od Wilczych Jagod");
        return false;
    }
}
