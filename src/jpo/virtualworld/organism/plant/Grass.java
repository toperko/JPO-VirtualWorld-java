package jpo.virtualworld.organism.plant;

import jpo.virtualworld.Parameter;
import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.World;

import java.awt.*;

public class Grass extends Plant {
    public Grass(World world, Coordinate coordinate) {
        super(world, coordinate, Parameter.GRASS_STRENGTH);
    }

    public Grass(Grass grass) {
        super(grass);
    }

    @Override
    public String getName() {
        return "Trawa";
    }

    @Override
    public Color getColor() {
        return Color.green;
    }

    @Override
    protected int getGrowthChance() {
        return Parameter.GRASS_GROWTH_CHANCE;
    }

    @Override
    public Organism newOrganism() {
        return new Grass(this);
    }
}
