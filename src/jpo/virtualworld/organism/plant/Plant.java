package jpo.virtualworld.organism.plant;

import jpo.virtualworld.organism.Organism;
import jpo.virtualworld.world.Coordinate;
import jpo.virtualworld.world.Direction;
import jpo.virtualworld.world.World;

import java.util.Random;

public abstract class Plant extends Organism implements Cloneable {

    public Plant(World world, Coordinate coordinate, int strength) {
        super(world, coordinate, strength, 0);
    }

    public Plant(Plant plant) {
        super(plant);
    }

    @Override
    public void move(Random randomGenerator) {
        if (!isGrowth(randomGenerator))
            return;

        int x, y, i = 0;
        try {
            do {
                x = this.coordinate.getWidth();
                y = this.coordinate.getHeight();
                switch (Direction.randomDirection(randomGenerator)) {
                    case up:
                        y -= 1;
                        break;
                    case right:
                        x += 1;
                        break;
                    case down:
                        y += 1;
                        break;
                    case left:
                        x -= 1;
                        break;
                }
            } while (++i < 4 && !(Coordinate.checkCoordinate(x, y) && !world.isSet(x, y)));
            if (!Coordinate.checkCoordinate(x, y) || world.isSet(x, y)) {
                return;
            }
            Organism newOrganism = this.newOrganism();
            newOrganism.getCoordinate().setWidth(x);
            newOrganism.getCoordinate().setHeight(y);
            world.addOrganism(newOrganism, x, y);
            world.addToCommunicates(getName() + " został/a zasiany/a.");
        } catch (Exception e) {
            world.addToCommunicates(e.toString());
        }
    }

    protected abstract int getGrowthChance();

    public abstract Organism newOrganism();

    protected boolean isGrowth(Random randomGenerator) {
        return (randomGenerator.nextInt(100) < getGrowthChance());
    }

    @Override
    public boolean collision(Organism attacker) {
        world.addToCommunicates(getName() + " zostala zjedzona przez " + attacker.getName());
        world.deleteOrganism(getCoordinate().getWidth(), getCoordinate().getHeight());
        return true;
    }
}
