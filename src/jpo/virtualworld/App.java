package jpo.virtualworld;

import jpo.virtualworld.world.World;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

public class App {
    private JButton infoButton;
    protected JPanel mainPanel;
    private WorldPanel worldMap;
    private JButton nextRound;
    private JTextPane textPane1;
    private JLabel roundCounter;
    private JLabel organismCounter;

    public App(World world) {
        this.worldMap.setWorld(world);
        organismCounter.setText(String.valueOf(world.getLivingOrganism()));
        SimpleAttributeSet keyWord = new SimpleAttributeSet();
        StyledDocument doc = textPane1.getStyledDocument();

        infoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Tomasz Perkowski 154943", "Toamsz Perkowski", 1);
            }
        });
        nextRound.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                world.nextRound();
                try{
                    doc.remove(0, doc.getLength());
                }catch (Exception ex){
                    System.out.println(ex);
                }


                for (Iterator<String> iterator = world.getCommunicates().listIterator(); iterator.hasNext(); ) {
                    String comunicate = iterator.next();
                    try {
                        doc.insertString(0, comunicate, null);
                    }catch (Exception ex){
                        System.out.println(ex);
                    }
                    iterator.remove();
                }
                organismCounter.setText(String.valueOf(world.getLivingOrganism()));
                roundCounter.setText(String.valueOf(world.getRound()));
                worldMap.repaint();
            }
        });

    }
}
