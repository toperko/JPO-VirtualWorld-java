package jpo.virtualworld;

public class Parameter {

    public static final int WORLD_HEIGHT = 20;
    public static final int WORLD_WIDTH = 20;

    public static final int WOLF_STRENGTH = 9;
    public static final int WOLF_INITIATIVE = 5;

    public static final int SHEEP_STRENGTH = 4;
    public static final int SHEEP_INITIATIVE = 4;

    public static final int ANTELOPE_STRENGTH = 4;
    public static final int ANTELOPE_INITIATIVE = 4;

    public static final int LION_STRENGTH = 11;
    public static final int LION_INITIATIVE = 7;

    public static final int MOSQUITO_STRENGTH = 1;
    public static final int MOSQUITO_INITIATIVE = 1;

    public static final int WOLFBERRY_STRENGTH = 2;
    public static final int WOLFBERRY_GROWTH_CHANCE = 20;
    public static final int THORN_STRENGTH = 0;
    public static final int THORN_GROWTH_CHANCE = 10;
    public static final int GRASS_STRENGTH = 0;
    public static final int GRASS_GROWTH_CHANCE = 30;

}
